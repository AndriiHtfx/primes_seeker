/*
 * Mock class for handler
 */

#ifndef LISTAT_TASK_TEST_H
#define LISTAT_TASK_TEST_H

#include "../../include/xml_handler.h"
#include <gmock/gmock.h>
#include <string>

class MockHandler : public xml::Handler {
public:
    MOCK_METHOD1(StartDoc, void(std::string filename));
    MOCK_METHOD0(EndDoc, void());
    MOCK_METHOD1(StartTag, void(std::string tag));
    MOCK_METHOD1(EndTag, void(std::string tag));
    MOCK_METHOD1(Content, void(std::string content));

};


#endif //LISTAT_TASK_TEST_H
