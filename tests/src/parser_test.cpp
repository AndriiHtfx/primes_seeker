/*
 * As it is only demo task I've decided not to perform
 * full test coverage
 */

#include "../include/moc_handler.h"
#include "../../include/xml_parser.h"
#include "../../include/xml_exception.h"
#include <gmock/gmock.h>
#include <gtest/gtest.h>

using ::testing::AtLeast;

TEST(ParserTest, CanHandleCorrectXML) {
    MockHandler test_handler;

    xml::Parser test_parser(&test_handler);
    test_parser.ParseFile("correct.xml");
    EXPECT_CALL(test_handler, StartDoc("correct.xml")).Times(1);
    EXPECT_CALL(test_handler, EndDoc()).Times(1);
    EXPECT_CALL(test_handler, StartTag("root")).Times(1);
    EXPECT_CALL(test_handler, StartTag("intervals")).Times(1);
    EXPECT_CALL(test_handler, StartTag("interval")).Times(1);
    EXPECT_CALL(test_handler, StartTag("low")).Times(1);
    EXPECT_CALL(test_handler, StartTag("high")).Times(1);
    EXPECT_CALL(test_handler, EndTag("low")).Times(1);
    EXPECT_CALL(test_handler, EndTag("high")).Times(1);
    EXPECT_CALL(test_handler, EndTag("interval")).Times(1);
    EXPECT_CALL(test_handler, EndTag("intervals")).Times(1);
    EXPECT_CALL(test_handler, EndTag("root")).Times(1);
}

TEST(ParserTest, CanHandleMalformedXML) {
    MockHandler test_handler;

    xml::Parser test_parser(&test_handler);

    ASSERT_THROW(test_parser.ParseFile("malformed.xml"), xml::Exception);
}
