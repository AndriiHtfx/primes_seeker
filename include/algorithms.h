//
// Algorithms for finding prime numbers
//

#ifndef LISTAT_TASK_ALGORITHMS_H
#define LISTAT_TASK_ALGORITHMS_H

namespace calc {

// Montgomery exponentiation algorithm
unsigned long PowMod(unsigned long base, unsigned long power, unsigned long modulo);
// Miller Rabin primality test
bool PrimalityTest(unsigned long prime, unsigned iterations);

} // namespace calc

#endif //LISTAT_TASK_ALGORITHMS_H
