/**
 * XML parser.
 * Uses SAX API provided by Handler. (Users can implement and use their own handlers with this parser.
 * Parses files with ASCII characters only.
 * Parser is not capable for parsing tag's attributes (this is not required by the task)
 * (This feature can be added by extending Parser and Handler, though)
 */

#ifndef LISTAT_TASK_SAXPARSER_H
#define LISTAT_TASK_SAXPARSER_H

#include "../include/xml_handler.h"
#include <string>

namespace xml {

class Parser {
public:
    explicit Parser(Handler *handler);

    void ParseFile(std::string filename);

private:
    Handler *handler;

    // states of the parser
    enum ParserState {
        idle         = 0, // doing nothing (expected to be only as initial state)
        pars_content = 1, // parsing text in tags : <tag> text </tag>
        pars_op_tag  = 2, // parsing opening tag : <tag>
        pars_cl_tag  = 3  // parsing closing tag : </tag>
    };
    // current parser's state
    ParserState state;

    // parser itself
    void Parse(std::string filename);
};

} // namespace xml

#endif //LISTAT_TASK_SAXPARSER_H
