/*
 * PrimesFinder was created to find primes in the given ranges.
 */

#ifndef LISTAT_TASK_PRIME_FINDER_H
#define LISTAT_TASK_PRIME_FINDER_H

#include <mutex>
#include <thread>
#include <vector>

namespace calc {

class PrimesFinder {
public:
    explicit PrimesFinder();
    ~PrimesFinder();

    void FindPrimes(unsigned long low, unsigned long high);
    std::vector <unsigned long> GetAllPrimesFound() const;

private:
    mutable std::mutex lock;
    mutable std::vector <std::thread> routines;
    mutable bool running_routines;

    std::vector <unsigned long> prime_numbers;

    static void PrimesSeekAlg(unsigned long low, unsigned long high, std::vector <unsigned long> &storage,
                              std::mutex &lock);
};

} // namespace calc

#endif //LISTAT_TASK_PRIME_FINDER_H
